RSpec.describe Constructor do
  describe 'database columns' do
    it { should have_db_column(:name).of_type(:string).with_options(null: false) }
    it { should have_db_column(:short_name).of_type(:string).with_options(null: false) }
    it { should have_db_column(:abbreviation).of_type(:string).with_options(null: false) }
    it { should have_db_column(:founded).of_type(:datetime) }
  end
end
