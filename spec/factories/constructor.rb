FactoryBot.define do
  factory :constructor do
    name { 'British Racing Motors' }
    short_name { 'BRM Formula 1' }
    abbreviation { 'BRM' }
  end
end
