class CreateConstructors < ActiveRecord::Migration[6.0]
  def change
    create_table :constructors do |t|
      t.string :name, null: false
      t.string :short_name, null: false
      t.datetime :founded
      t.string :abbreviation, null: false

      t.timestamps
    end
  end
end
